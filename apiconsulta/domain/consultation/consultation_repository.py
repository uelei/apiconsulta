from abc import ABC, abstractmethod
from typing import List, Optional

from domain.consultation import Consultation

from uuid import UUID


class ConsultationAbsRepository(ABC):
    """Define a Abstract repo for Consultation"""

    @abstractmethod
    def create(self, consultation: Consultation) -> Optional[Consultation]:
        raise NotImplementedError

    @abstractmethod
    def find_by_id(self, id: UUID) -> Optional[Consultation]:
        raise NotImplementedError

    @abstractmethod
    def find_by_physician_id(self, physician_id: UUID) -> Optional[Consultation]:
        raise NotImplementedError

    @abstractmethod
    def find_by_patient_id(self, patient_id: UUID) -> Optional[Consultation]:
        raise NotImplementedError

    @abstractmethod
    def update(self, consultation: Consultation) -> Optional[Consultation]:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, id: UUID):
        raise NotImplementedError

    @abstractmethod
    def set_consultation_as_sent(self, id: UUID) -> bool:
        raise NotImplementedError
