from typing import Optional, Union
from datetime import datetime
import uuid

from domain.consultation.const import CONSULTATION_HOUR_PRICE


class Consultation:
    """Consultation represents a doctor appointment."""

    def __init__(
        self,
        id: uuid,
        physician_id: uuid,
        patient_id: uuid,
        start_date: Union[datetime, str],
        end_date: Optional[Union[datetime, str]] = None,
        price: Optional[float] = None,
        sent: Optional[bool] = False,
    ):
        self.id: uuid = id
        self.physician_id: uuid = physician_id
        self.patient_id: uuid = patient_id
        self.start_date: datetime = start_date
        self.end_date: Optional[datetime] = end_date
        self.price: Optional[float] = price
        self.sent: Optional[bool] = sent
        self.calculate_price()

    def is_already_finished(self) -> bool:
        return self.end_date is not None

    def calculate_price(self) -> None:
        if self.end_date and not self.price:
            self.price = (
                int(round((self.end_date - self.start_date).total_seconds() / 3600, 0))
                * CONSULTATION_HOUR_PRICE
            )
