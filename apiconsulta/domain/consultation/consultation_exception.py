class ConsultationNotFoundError(Exception):
    message = "The consultation  you spcecified does not exist."

    def __str__(self):
        return ConsultationNotFoundError.message


class ConsultationAlreadyExistsError(Exception):
    message = "The Consultation with the id you specified already exists."

    def __str__(self):
        return ConsultationAlreadyExistsError.message


class ConsultationAlreadySentError(Exception):
    message = "The Consultation with the id you specified is already send."

    def __str__(self):
        return ConsultationAlreadyExistsError.message