import logging
from typing import Iterator

from fastapi import Depends, FastAPI, HTTPException, status
from sqlalchemy.orm.session import Session

from domain.consultation import (
    ConsultationAbsRepository,
    ConsultationAlreadyExistsError,
    ConsultationNotFoundError,
    ConsultationAlreadySentError,
)
from infrastructure.postgres.consultation.consultation_query_service import (
    ConsultationQueryServiceImpl,
)
from infrastructure.postgres.consultation.consultation_repository import (
    ConsultationRepositoryImpl,
    ConsultationCommandUseCaseUnitOfWorkImpl,
)
from infrastructure.postgres.database import create_tables, SessionLocal
from presentation.schema.consultation.consultation_error_message import (
    ErrorMessageConsultationAlreadyExists,
    ErrorMessageConsultationNotFound,
)
from usecase.consultation import ConsultationReadModel
from usecase.consultation.consultation_command_model import (
    ConsultationCreateModel,
    ConsultationUpdateModel,
)
from usecase.consultation.consultation_command_usecase import (
    ConsultationCommandUseCase,
    ConsultationCommandUseCaseUnitOfWork,
    ConsultationCommandUseCaseImpl,
)
from usecase.consultation.consultation_query_service import (
    ConsultationQueryService,
)
from usecase.consultation.consultation_query_usecase import (
    ConsultationQueryUseCase,
    ConsultationQueryUseCaseImpl,
)

# config.fileConfig("./apiconsulta/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)

app = FastAPI()

create_tables()


def get_session() -> Iterator[Session]:
    session: Session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


def consultation_query_usecase(
    session: Session = Depends(get_session),
) -> ConsultationQueryUseCase:
    consultation_query_service: ConsultationQueryService = ConsultationQueryServiceImpl(
        session
    )
    return ConsultationQueryUseCaseImpl(consultation_query_service)


def consultation_command_usecase(
    session: Session = Depends(get_session),
) -> ConsultationCommandUseCase:
    consultation_repository: ConsultationAbsRepository = ConsultationRepositoryImpl(
        session
    )
    uow: ConsultationCommandUseCaseUnitOfWork = (
        ConsultationCommandUseCaseUnitOfWorkImpl(
            session, consultation_repository=consultation_repository
        )
    )
    return ConsultationCommandUseCaseImpl(uow)


@app.post(
    "/consultation",
    response_model=ConsultationReadModel,
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_409_CONFLICT: {
            "model": ErrorMessageConsultationAlreadyExists,
        },
    },
)
async def create_consultation(
    data: ConsultationCreateModel,
    consultation_command_usecase: ConsultationCommandUseCase = Depends(
        consultation_command_usecase
    ),
):
    try:
        consultation = consultation_command_usecase.create_consultation(data)
    except ConsultationAlreadyExistsError as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=e.message,
        )
    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    return consultation


@app.get(
    "/consultation/{consultation_id}",
    response_model=ConsultationReadModel,
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_404_NOT_FOUND: {
            "model": ErrorMessageConsultationNotFound,
        },
    },
)
async def get_consultation(
    consultation_id: str,
    consultation_query_usecase: ConsultationQueryUseCase = Depends(
        consultation_query_usecase
    ),
):
    try:
        consultation = consultation_query_usecase.fetch_consultation_by_id(
            consultation_id
        )
    except ConsultationNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=e.message,
        )
    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    return consultation


@app.put(
    "/consultation/{consultation_id}",
    response_model=ConsultationReadModel,
    status_code=status.HTTP_202_ACCEPTED,
    responses={
        status.HTTP_404_NOT_FOUND: {
            "model": ErrorMessageConsultationNotFound,
        },
    },
)
async def update_consultation(
    consultation_id: str,
    data: ConsultationUpdateModel,
    consultation_command_usecase: ConsultationCommandUseCase = Depends(
        consultation_command_usecase
    ),
):
    try:
        updated_consultation = consultation_command_usecase.update_consultation(
            consultation_id, data
        )
    except ConsultationNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=e.message,
        )
    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    return updated_consultation


@app.delete(
    "/consultation/{consultation_id}",
    status_code=status.HTTP_202_ACCEPTED,
    responses={
        status.HTTP_404_NOT_FOUND: {
            "model": ErrorMessageConsultationNotFound,
        },
    },
)
async def delete_consultation(
    consultation_id: str,
    consultation_command_usecase: ConsultationCommandUseCase = Depends(
        consultation_command_usecase
    ),
):
    try:
        consultation_command_usecase.delete_consultation_by_id(consultation_id)
    except ConsultationNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=e.message,
        )
    except ConsultationAlreadySentError as e:
        raise HTTPException(
            status_code=status.HTTP_424_FAILED_DEPENDENCY,
            detail=e.message,
        )
    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
