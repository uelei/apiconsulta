from unittest.mock import MagicMock, Mock

import pytest

from domain.consultation import ConsultationNotFoundError
from infrastructure.postgres.consultation import ConsultationQueryServiceImpl
from usecase.consultation import ConsultationQueryUseCaseImpl, ConsultationReadModel


class TestConsultationQueryUseCase:
    def test_fetch_consultation_by_id_should_return_book(self):

        session = MagicMock()
        consultation_query_service = ConsultationQueryServiceImpl(session)
        consultation_query_service.find_by_id = Mock(
            return_value=ConsultationReadModel(
                id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                start_date="2021-08-18 13:00:00",
                end_date="2021-08-19 14:00:00",
                physician_id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                patient_id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                price=200.00,
                sent=False,
            )
        )

        consultation_query_usecase = ConsultationQueryUseCaseImpl(
            consultation_query_service
        )

        consultation = consultation_query_usecase.fetch_consultation_by_id(
            "84ab6121-c4a8-4684-8cc2-b03024ec0f1d"
        )

        assert str(consultation.id) == "84ab6121-c4a8-4684-8cc2-b03024ec0f1d"

    def test_fetch_all_consultations_not_sent(self):
        session = MagicMock()
        consultation_query_service = ConsultationQueryServiceImpl(session)
        consultation_query_service.fetch_all_consultations_not_sent = Mock(
            return_value=[
                ConsultationReadModel(
                    id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                    start_date="2021-08-18 13:00:00",
                    end_date="2021-08-19 14:00:00",
                    physician_id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                    patient_id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                    price=200.00,
                    sent=False,
                ),
            ]
        )

        consultation_query_usecase = ConsultationQueryUseCaseImpl(
            consultation_query_service
        )
        consultations = consultation_query_usecase.fetch_all_consultations_not_sent()

        assert len(consultations) == 1
        assert consultations[0].price == 200

    def test_fetch_consultation_by_id_should_throw_consultation_not_found_error(self):

        session = MagicMock()
        consultation_query_service = ConsultationQueryServiceImpl(session)
        consultation_query_service.find_by_id = Mock(
            side_effect=ConsultationNotFoundError
        )

        consultation_query_usecase = ConsultationQueryUseCaseImpl(
            consultation_query_service
        )

        with pytest.raises(ConsultationNotFoundError):
            consultation_query_usecase.fetch_consultation_by_id(
                "cPqw4yPVUM3fA9sqzpZmkL"
            )

    def test_fetch_all(self):
        session = MagicMock()
        consultation_query_service = ConsultationQueryServiceImpl(session)
        consultation_query_service.fetch_all_consultations_not_sent = Mock(
            return_value=[
                ConsultationReadModel(
                    id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                    start_date="2021-08-18 13:00:00",
                    end_date="2021-08-19 14:00:00",
                    physician_id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                    patient_id="84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
                    price=200.00,
                    sent=True,
                )
            ]
        )

        consultation_query_usecase = ConsultationQueryUseCaseImpl(
            consultation_query_service
        )
        consultations = consultation_query_usecase.fetch_all_consultations_not_sent()

        assert len(consultations) == 1
        assert consultations[0].price == 200
