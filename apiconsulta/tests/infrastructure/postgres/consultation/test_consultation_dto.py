import uuid
from datetime import datetime

import pytest

from domain.consultation import Consultation
from infrastructure.postgres.consultation import ConsultationDTO


class TestConsultationDTO:
    def test_to_read_model_should_create_entity_instance(self):

        generated_uuid = uuid.uuid4()
        patient_id = uuid.uuid4()
        physician_id = uuid.uuid4()
        start_date = datetime.now().isoformat()

        consultation_dto = ConsultationDTO(
            id=generated_uuid,
            physician_id=physician_id,
            patient_id=patient_id,
            start_date=start_date,
        )

        consultation = consultation_dto.to_read_model()

        assert consultation.id == generated_uuid
        assert consultation.patient_id == patient_id
        assert consultation.physician_id == physician_id

    def test_to_entity_should_create_entity_instance(self):
        generated_uuid = uuid.uuid4()
        patient_id = uuid.uuid4()
        physician_id = uuid.uuid4()
        start_date = datetime.now().isoformat()

        consultation_dto = ConsultationDTO(
            id=generated_uuid,
            physician_id=physician_id,
            patient_id=patient_id,
            start_date=start_date,
        )

        consultation = consultation_dto.to_entity()

        assert consultation.id == generated_uuid
        assert consultation.patient_id == patient_id
        assert consultation.physician_id == physician_id

    def test_from_entity_should_create_dto_instance(self):

        generated_uuid = uuid.uuid4()
        patient_id = uuid.uuid4()
        physician_id = uuid.uuid4()
        start_date = datetime.now().isoformat()

        consultation = Consultation(
            id=generated_uuid,
            physician_id=physician_id,
            patient_id=patient_id,
            start_date=start_date,
        )

        consultation_dto = ConsultationDTO.from_entity(consultation)

        assert consultation_dto.id == generated_uuid
        assert consultation_dto.patient_id == patient_id
        assert consultation_dto.physician_id == physician_id
