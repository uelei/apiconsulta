import uuid
from datetime import datetime

import pytest
from domain.consultation import Consultation


class TestConsultation:
    def test_constructor_should_create_instance(self):
        generated_uuid = uuid.uuid4()
        patient_id = uuid.uuid4()
        physician_id = uuid.uuid4()
        start_date = datetime.now().isoformat()
        consultation = Consultation(
            id=generated_uuid,
            physician_id=physician_id,
            patient_id=patient_id,
            start_date=start_date,
        )

        assert consultation.id == generated_uuid
        assert consultation.patient_id == patient_id
        assert consultation.physician_id == physician_id
        assert consultation.start_date == start_date
