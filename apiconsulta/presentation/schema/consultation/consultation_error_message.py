from pydantic import BaseModel, Field

from domain.consultation import (
    ConsultationNotFoundError,
    ConsultationAlreadyExistsError,
)


class ErrorMessageConsultationNotFound(BaseModel):
    detail: str = Field(example=ConsultationNotFoundError.message)


class ErrorMessageConsultationAlreadyExists(BaseModel):
    detail: str = Field(example=ConsultationAlreadyExistsError.message)
