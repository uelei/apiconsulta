import logging
import os
import requests
from celery import Celery

from infrastructure.postgres.database import SessionLocal
from main import (
    consultation_query_usecase as consultation_query_usecase_factory,
)
from main import (
    consultation_command_usecase as consultation_command_usecase_factory,
)

logger = logging.getLogger("invoice_sender")

REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379/0")

app = Celery()
app.conf.broker_url = REDIS_URL
app.conf.result_backend = REDIS_URL
app.conf.timezone = "UTC"


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls invoice sender every 30 seconds
    sender.add_periodic_task(30.0, invoice_sender_spawn.s())


@app.task()
def invoice_sender_spawn():

    # get all consultations that was not sent
    session = SessionLocal()
    consultation_query_usecase = consultation_query_usecase_factory(session)

    consultations = consultation_query_usecase.fetch_all_consultations_not_sent()

    for consultation in consultations:
        invoice_sender_by_id.delay(str(consultation.id))
    logger.info("all invoices sent.")


@app.task(retry_backoff=True)
def invoice_sender_by_id(consultation_id):

    session = SessionLocal()
    consultation_query_usecase = consultation_query_usecase_factory(session)

    consultation = consultation_query_usecase.fetch_consultation_by_id(consultation_id)

    consultation_command_usecase = consultation_command_usecase_factory(session)
    try:
        logger.debug(f"sending compensation id {consultation.id}")
        # send request !
        data_post = {
            "appointment_id": str(consultation.id),
            "total_price": consultation.price,
        }
        print(data_post)
        requests.post(
            os.getenv("API_INVOICE_URL", "http://localhost:8000") + "/invoice",
            json=data_post,
        )
        consultation_command_usecase.update_consultation_to_sent(str(consultation.id))
    except Exception as e:
        logger.error(e)
        raise
