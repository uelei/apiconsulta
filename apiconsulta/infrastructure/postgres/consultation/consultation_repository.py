from typing import Optional
from uuid import UUID

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.session import Session

from domain.consultation.consultation import Consultation
from domain.consultation.consultation_repository import (
    ConsultationAbsRepository,
)
from usecase.consultation import ConsultationCommandUseCaseUnitOfWork
from .consultation_dto import ConsultationDTO


class ConsultationRepositoryImpl(ConsultationAbsRepository):
    """ConsultationRepositoryImpl implements CRUD operations related Consultation entity using SQLAlchemy."""

    def __init__(self, session: Session):
        self.session: Session = session

    def find_by_id(self, id: UUID) -> Optional[Consultation]:
        try:
            consultation_dto = (
                self.session.query(ConsultationDTO).filter_by(id=id).one()
            )
        except NoResultFound:
            return None
        except:
            raise

        return consultation_dto.to_entity()

    def find_by_physician_id(self, physician_id: str) -> Optional[Consultation]:
        try:
            consultation_dto = (
                self.session.query(ConsultationDTO)
                .filter_by(physician_id=physician_id)
                .one()
            )
        except NoResultFound:
            return None
        except:
            raise

        return consultation_dto.to_entity()

    def find_by_patient_id(self, patient_id: str) -> Optional[Consultation]:
        try:
            consultation_dto = (
                self.session.query(ConsultationDTO)
                .filter_by(patient_id=patient_id)
                .one()
            )
        except NoResultFound:
            return None
        except:
            raise

        return consultation_dto.to_entity()

    def create(self, consultation: Consultation):
        consultation_dto = ConsultationDTO.from_entity(consultation)
        try:
            self.session.add(consultation_dto)
        except:
            raise

    def update(self, consultation: Consultation):
        consultation_dto = ConsultationDTO.from_entity(consultation)
        try:
            _consultation = (
                self.session.query(ConsultationDTO)
                .filter_by(id=consultation_dto.id)
                .one()
            )
            _consultation.start_date = consultation_dto.start_date
            _consultation.end_date = consultation_dto.end_date
            _consultation.price = consultation_dto.price
        except:
            raise

    def delete_by_id(self, id: str):
        try:
            self.session.query(ConsultationDTO).filter_by(id=id).delete()
        except:
            raise

    def set_consultation_as_sent(self, id: str) -> bool:
        try:
            _consultation = self.session.query(ConsultationDTO).filter_by(id=id).one()
            _consultation.sent = True
        except:
            raise
        return True


class ConsultationCommandUseCaseUnitOfWorkImpl(ConsultationCommandUseCaseUnitOfWork):
    def __init__(
        self,
        session: Session,
        consultation_repository: ConsultationAbsRepository,
    ):
        self.session: Session = session
        self.consultation_repository: ConsultationAbsRepository = (
            consultation_repository
        )

    def begin(self):
        self.session.begin()

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()
