from typing import List, Optional

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.session import Session

from .consultation_dto import ConsultationDTO
from usecase.consultation import (
    ConsultationReadModel,
    ConsultationQueryService,
)


class ConsultationQueryServiceImpl(ConsultationQueryService):
    """ConsultationQueryServiceImpl implements READ operations related Consultation entity using SQLAlchemy."""

    def __init__(self, session: Session):
        self.session: Session = session

    def find_by_id(self, id: str) -> Optional[ConsultationReadModel]:
        try:
            consultation_dto = (
                self.session.query(ConsultationDTO).filter_by(id=id).one()
            )
        except NoResultFound:
            return None
        except:
            raise

        return consultation_dto.to_read_model()

    def find_all(self) -> List[ConsultationReadModel]:
        try:
            consultation_dtos = (
                self.session.query(ConsultationDTO)
                .order_by(ConsultationDTO.start_date)
                .limit(100)
                .all()
            )
        except:
            raise

        if len(consultation_dtos) == 0:
            return []

        return list(
            map(
                lambda consultation_dto: consultation_dto.to_read_model(),
                consultation_dtos,
            )
        )

    def fetch_all_consultations_not_sent(self) -> List[ConsultationReadModel]:
        try:
            consultation_dtos = (
                self.session.query(ConsultationDTO)
                .filter(ConsultationDTO.sent == False, ConsultationDTO.end_date != None)
                .order_by(ConsultationDTO.start_date)
                .all()
            )
        except:
            raise

        if len(consultation_dtos) == 0:
            return []

        return list(
            map(
                lambda consultation_dto: consultation_dto.to_read_model(),
                consultation_dtos,
            )
        )
