import uuid
from datetime import datetime
from typing import Union

from sqlalchemy import Column, DateTime, Float, Boolean
from sqlalchemy.dialects.postgresql import UUID

from domain.consultation import Consultation
from infrastructure.postgres.database import Base
from usecase.consultation import ConsultationReadModel


class ConsultationDTO(Base):
    """ConsultationDTO is a data transfer object associated with Consultation entity."""

    __tablename__ = "consultation"
    id: Union[uuid.UUID, Column] = Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4
    )
    physician_id: Union[uuid.UUID, Column] = Column(UUID(as_uuid=True), nullable=False)
    patient_id: Union[uuid.UUID, Column] = Column(UUID(as_uuid=True), nullable=False)
    start_date: Union[datetime, Column] = Column(
        DateTime, default=datetime.now, nullable=False
    )
    end_date: Union[datetime, Column] = Column(DateTime)
    price: Union[float, Column] = Column(Float)
    sent: Union[bool, Column] = Column(Boolean, default=False)

    def to_entity(self) -> Consultation:
        return Consultation(
            id=self.id,
            physician_id=self.physician_id,
            patient_id=self.patient_id,
            start_date=self.start_date,
            end_date=self.end_date,
            price=self.price,
            sent=self.sent,
        )

    def to_read_model(self) -> ConsultationReadModel:
        return ConsultationReadModel(
            id=self.id,
            start_date=self.start_date,
            end_date=self.end_date,
            physician_id=self.physician_id,
            patient_id=self.patient_id,
            price=self.price,
            sent=self.sent,
        )

    @staticmethod
    def from_entity(consultation: Consultation) -> "ConsultationDTO":
        return ConsultationDTO(
            id=consultation.id,
            physician_id=consultation.physician_id,
            patient_id=consultation.patient_id,
            start_date=consultation.start_date,
            end_date=consultation.end_date,
            price=consultation.price,
            sent=consultation.sent,
        )
