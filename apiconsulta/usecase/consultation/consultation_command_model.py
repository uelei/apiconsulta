from typing import Optional
from uuid import UUID
from datetime import datetime
from pydantic import BaseModel, Field, validator
from dateutil import parser


class ConsultationCreateModel(BaseModel):
    """ConsultationCreateModel represents a write model to create a consultation."""

    physician_id: UUID = Field(example="84ab6121-c4a8-4684-8cc2-b03024ec0f1d")
    patien_id: UUID = Field(example="84ab6121-c4a8-4684-8cc2-b03024ec0f1d")
    start_date: datetime = Field(example="2021-08-18 13:00:00")

    @validator("start_date", pre=True)
    def parse_start_date(cls, value):
        return parser.parse(value)


class ConsultationUpdateModel(BaseModel):
    """ConsultationUpdateModel represents a write model to update a consultation."""

    start_date: Optional[datetime] = Field(example="2021-08-18 12:00:00")
    end_date: datetime = Field(example="2021-08-18 13:00:00")
    price: Optional[float] = Field(example="200.00")

    @validator("start_date", "end_date", pre=True)
    def parse_start_date(cls, value):
        return parser.parse(value)
