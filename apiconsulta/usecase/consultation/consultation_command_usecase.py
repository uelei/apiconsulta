from abc import ABC, abstractmethod
from typing import Optional, cast

import uuid

from .consultation_command_model import ConsultationCreateModel, ConsultationUpdateModel
from .consultation_query_model import ConsultationReadModel
from domain.consultation import (
    ConsultationAbsRepository,
    Consultation,
    ConsultationNotFoundError,
    ConsultationAlreadyExistsError,
    ConsultationAlreadySentError
)


class ConsultationCommandUseCaseUnitOfWork(ABC):
    """ConsultationCommandUseCaseUnitOfWork defines an interface based on Unit of Work pattern."""

    consultation_repository: ConsultationAbsRepository

    @abstractmethod
    def begin(self):
        raise NotImplementedError

    @abstractmethod
    def commit(self):
        raise NotImplementedError

    @abstractmethod
    def rollback(self):
        raise NotImplementedError


class ConsultationCommandUseCase(ABC):
    """ConsultationCommandUseCase defines a command usecase inteface related Consultation entity."""

    @abstractmethod
    def create_consultation(
        self, data: ConsultationCreateModel
    ) -> Optional[ConsultationReadModel]:
        raise NotImplementedError

    @abstractmethod
    def update_consultation(
        self, id: str, data: ConsultationUpdateModel
    ) -> Optional[ConsultationReadModel]:
        raise NotImplementedError

    @abstractmethod
    def update_consultation_to_sent(self, id: str):
        raise NotImplementedError

    @abstractmethod
    def delete_consultation_by_id(self, id: str):
        raise NotImplementedError


class ConsultationCommandUseCaseImpl(ConsultationCommandUseCase):
    """ConsultationCommandUseCaseImpl implements a command usecases related Consultation entity."""

    def __init__(
        self,
        uow: ConsultationCommandUseCaseUnitOfWork,
    ):
        self.uow: ConsultationCommandUseCaseUnitOfWork = uow

    def create_consultation(
        self, data: ConsultationCreateModel
    ) -> Optional[ConsultationReadModel]:
        try:
            uuid_id = uuid.uuid4()

            consultation = Consultation(
                id=uuid_id,
                physician_id=data.physician_id,
                patient_id=data.patien_id,
                start_date=data.start_date,
            )

            existing_consultation = self.uow.consultation_repository.find_by_id(uuid_id)
            if existing_consultation is not None:
                raise ConsultationAlreadyExistsError

            self.uow.consultation_repository.create(consultation)
            self.uow.commit()

            created_consultation = self.uow.consultation_repository.find_by_id(uuid_id)
        except:
            self.uow.rollback()
            raise

        return ConsultationReadModel.from_entity(
            cast(Consultation, created_consultation)
        )

    def update_consultation(
        self, id: str, data: ConsultationUpdateModel
    ) -> Optional[ConsultationReadModel]:
        try:
            existing_consultation = self.uow.consultation_repository.find_by_id(id)
            if existing_consultation is None:
                raise ConsultationNotFoundError

            consultation = Consultation(
                id=id,
                physician_id=existing_consultation.physician_id,
                patient_id=existing_consultation.patient_id,
                end_date=data.end_date,
                price=data.price,
                start_date=data.start_date or existing_consultation.start_date,
            )

            self.uow.consultation_repository.update(consultation)

            updated_consultation = self.uow.consultation_repository.find_by_id(
                consultation.id
            )

            self.uow.commit()
        except:
            self.uow.rollback()
            raise

        return ConsultationReadModel.from_entity(
            cast(Consultation, updated_consultation)
        )

    def update_consultation_to_sent(self, id: str) -> Optional[ConsultationReadModel]:
        try:
            existing_consultation = self.uow.consultation_repository.find_by_id(id)
            if existing_consultation is None:
                raise ConsultationNotFoundError

            existing_consultation.sent = True

            self.uow.consultation_repository.set_consultation_as_sent(
                existing_consultation.id
            )

            updated_consultation = self.uow.consultation_repository.find_by_id(
                existing_consultation.id
            )

            self.uow.commit()
        except:
            self.uow.rollback()
            raise

        return ConsultationReadModel.from_entity(
            cast(Consultation, updated_consultation)
        )

    def delete_consultation_by_id(self, id: str):
        try:
            existing_consultation = self.uow.consultation_repository.find_by_id(id)
            if existing_consultation is None:
                raise ConsultationNotFoundError
            
            elif existing_consultation.sent:
                raise ConsultationAlreadySentError

            self.uow.consultation_repository.delete_by_id(id)

            self.uow.commit()
        except:
            self.uow.rollback()
            raise
