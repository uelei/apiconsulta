from abc import ABC, abstractmethod
from typing import List, Optional

from .consultation_query_model import ConsultationReadModel


class ConsultationQueryService(ABC):
    """ConsultationQueryService defines a query service inteface related consultation entity."""

    @abstractmethod
    def find_by_id(self, id: str) -> Optional[ConsultationReadModel]:
        raise NotImplementedError

    @abstractmethod
    def fetch_all_consultations_not_sent(self) -> List[ConsultationReadModel]:
        raise NotImplementedError
