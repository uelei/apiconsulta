from abc import ABC, abstractmethod
from typing import List, Optional

from .consultation_query_model import ConsultationReadModel
from domain.consultation import (
    ConsultationNotFoundError,
    ConsultationAlreadyExistsError,
)
from .consultation_query_service import ConsultationQueryService


class ConsultationQueryUseCase(ABC):
    """ConsultationQueryUseCase defines a query usecase inteface related consultation entity."""

    @abstractmethod
    def fetch_consultation_by_id(self, id: str) -> Optional[ConsultationReadModel]:
        raise NotImplementedError

    @abstractmethod
    def fetch_all_consultations_not_sent(self) -> Optional[List[ConsultationReadModel]]:
        raise NotImplementedError


class ConsultationQueryUseCaseImpl(ConsultationQueryUseCase):
    """ConsultationQueryUseCaseImpl implements a query usecases related Consultation entity."""

    def __init__(self, consultation_query_service: ConsultationQueryService):
        self.consultation_query_service: ConsultationQueryService = (
            consultation_query_service
        )

    def fetch_consultation_by_id(self, id: str) -> Optional[ConsultationReadModel]:
        try:
            consultation = self.consultation_query_service.find_by_id(id)
            if consultation is None:
                raise ConsultationNotFoundError
        except:
            raise

        return consultation

    def fetch_all_consultations_not_sent(self) -> Optional[List[ConsultationReadModel]]:
        try:
            consultations = (
                self.consultation_query_service.fetch_all_consultations_not_sent()
            )
            if consultations is None:
                raise ConsultationNotFoundError
        except:
            raise

        return consultations
