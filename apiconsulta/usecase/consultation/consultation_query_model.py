from datetime import datetime
from typing import cast, Optional
from uuid import UUID

from pydantic import BaseModel, Field

from domain.consultation import Consultation


class ConsultationReadModel(BaseModel):
    """ConsultationReadModel represents data structure as a read model."""

    id: UUID = Field(example="84ab6121-c4a8-4684-8cc2-b03024ec0f1d")
    start_date: datetime = Field(example="2021-08-18 13:00:00")
    end_date: Optional[datetime] = Field(example="2021-08-18 14:00:00")
    physician_id: UUID = Field(example="84ab6121-c4a8-4684-8cc2-b03024ec0f1d")
    patient_id: UUID = Field(example="84ab6121-c4a8-4684-8cc2-b03024ec0f1d")
    price: Optional[float] = Field(example=200.00)
    sent: Optional[bool] = Field(example=False)

    class Config:
        orm_mode = True

    @staticmethod
    def from_entity(consultation: Consultation) -> "ConsultationReadModel":
        return ConsultationReadModel(
            id=consultation.id,
            start_date=consultation.start_date,
            end_date=consultation.end_date,
            physician_id=consultation.physician_id,
            patient_id=consultation.patient_id,
            price=consultation.price,
            sent=consultation.sent,
        )
