POETRY=poetry
PYTEST=$(POETRY) run pytest
UVICORN=$(POETRY) run uvicorn
PACKAGE=all_apis

update:
	$(POETRY) update

test: 
	$(PYTEST) -vv

dev-consultation:
	${UVICORN}  apiconsulta.main:app --reload

dev-invoice:
	${UVICORN}  invoice_api.main:app --reload
