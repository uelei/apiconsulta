# Api Consulta + Api Invoice

## Tech Stack

- [FastAPI](https://fastapi.tiangolo.com/)
- [SQLAlchemy](https://www.sqlalchemy.org/)
  - [Postgres](https://www.postgresql.org/)
- [Celery](https://docs.celeryproject.org/en/stable/)
- [Poetry](https://python-poetry.org/)
- [Docker](https://www.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/install/)

## Code Architecture

- Api 1 Consulta
  Baseada em uma arquitura em camadas [Onion Architecture](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/)  
  Codigo estruturado usando o padrão DDD

- Api 2 Invoice
  Baseada em microserviços [Microservices](https://www.fullstackpython.com/microservices.html)

## Como executar

1. Clonar o repositório
2. Buildar as imagens do docker-compose

    ```bash
    docker-compose build
    ```

3. Executar o docker-compose

    ```bash
    docker-compose up
    ```

4. Acessar a API 1 documentaçao http://127.0.0.1:8000/docs
5. Acessar a API 2 documentaçao http://127.0.0.1:8800/docs

## Funcionamento

### Criar uma nova consulta

```bash
curl --location --request POST 'localhost:8000/consultation' \
--header 'Content-Type: application/json' \
--data-raw '{
  "physician_id": "84ab6121-d4a8-4684-8cc2-b03024ec0f1d",
  "patien_id": "84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
  "start_date": "2021-08-18 13:00:00"
}'
```

- Resposta do Request:

```json
{
  "id": "e17d8909-bad8-4109-b507-b896346bb65f",
  "start_date": "2021-08-18T13:00:00",
  "end_date": null,
  "physician_id": "84ab6121-d4a8-4684-8cc2-b03024ec0f1d",
  "patient_id": "84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
  "price": null,
  "sent": false
}
```

ps. A consulta foi iniciada mas não foi concluída

### Consultar uma consulta

```bash
curl --location --request GET 'localhost:8000/consultation/e17d8909-bad8-4109-b507-b896346bb65f'
```

- Resposta do GET request:

```json
{
  "id": "e17d8909-bad8-4109-b507-b896346bb65f",
  "start_date": "2021-08-18T13:00:00",
  "end_date": null,
  "physician_id": "84ab6121-d4a8-4684-8cc2-b03024ec0f1d",
  "patient_id": "84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
  "price": null,
  "sent": false
}
```

### Concluir uma consulta

O price pode ser omitdo para o calculo, se enviado será considerado o valor enviado.
Pode tb se editar o horário de inicio e de fim
o unico requisto obrigatório é o horário de fim`end_date`

```bash
curl --location --request POST 'localhost:8000/consultation/e17d8909-bad8-4109-b507-b896346bb65f' \
--header 'Content-Type: application/json' \
--data-raw '{
  "start_date": "2021-08-18 12:00:00",
  "end_date": "2021-08-18 13:00:00"
}'
```

- Resposta do Request:

```json
{
  "id": "e17d8909-bad8-4109-b507-b896346bb65f",
  "start_date": "2021-08-18T12:00:00",
  "end_date": "2021-08-18T13:00:00",
  "physician_id": "84ab6121-d4a8-4684-8cc2-b03024ec0f1d",
  "patient_id": "84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
  "price": 200,
  "sent": false
}
```

### Processamento de uma consulta

Após um período de 30 segundos, é disparado um worker que faz uma pesquisa do banco de dados,
buscando todas as consultas finalizadas e não enviadas, e colocando na fila uma tarefa para cada consulta não enviada.
Caso a api 2 esteja sobrecarregada temos uma opcão configurada de retrys exponenciais. [backoff_retry](https://docs.celeryproject.org/en/stable/userguide/tasks.html#Task.retry_backoff)

### Consultando uma Invoice pelo id da consulta

```bash
curl --location --request GET 'localhost:8800/invoice_by_appointment/e17d8909-bad8-4109-b507-b896346bb65f'
```

- Resposta do GET request:

```json
{
  "id": "7804217e-fd55-4505-92e9-dd161a58d698",
  "appointment_id": "e17d8909-bad8-4109-b507-b896346bb65f",
  "total_price": 200
}
```
