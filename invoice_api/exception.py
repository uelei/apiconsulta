class InvoiceAlreadyExistsError(Exception):
    message = "The Invoice with the id you specified already exists."

    def __str__(self):
        return InvoiceAlreadyExistsError.message
