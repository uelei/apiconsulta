import logging
import uuid
from typing import Optional

import uvicorn
from fastapi import FastAPI, Depends, HTTPException
from pydantic import BaseModel, validator
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from controller import (
    get_invoice_details_from_db,
    save_invoice_to_db,
    get_invoice_by_appointment_from_db,
)
from exception import InvoiceAlreadyExistsError
from infrastructure.database import get_session, engine, Base
from infrastructure.model import Invoice

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class InvoiceReadModel(BaseModel):
    id: Optional[str]
    appointment_id: str
    total_price: float

    @validator("appointment_id", pre=True)
    def parse_appointment_id(cls, value):
        try:
            uuid.UUID(value)
        except:
            raise
        return value

    @validator("id", pre=True)
    def parse_id(cls, value):
        if value:
            try:
                uuid.UUID(value)
            except:
                raise
        return value

    @classmethod
    def from_orm(cls, *args, **kwargs):
        return InvoiceReadModel(
            id=str(kwargs["id"]),
            appointment_id=str(kwargs["appointment_id"]),
            total_price=kwargs["total_price"],
        )


"""Load app and initialize database"""
app = FastAPI()


@app.on_event("startup")
async def startup():
    # create db tables
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


@app.get("/invoice/{id}", response_model=InvoiceReadModel)
async def read_invoice_details(
    session: AsyncSession = Depends(get_session), id: str = None
):
    invoice: Invoice = await get_invoice_details_from_db(session, id)

    return InvoiceReadModel.from_orm(**invoice.__dict__) if invoice else None


@app.get("/invoice_by_appointment/{appointment_id}", response_model=InvoiceReadModel)
async def read_invoice_details_by_invoice_by_appointment(
    session: AsyncSession = Depends(get_session), appointment_id: str = None
):
    invoice: Invoice = await get_invoice_by_appointment_from_db(session, appointment_id)

    return InvoiceReadModel.from_orm(**invoice.__dict__)


@app.post("/invoice", response_model=InvoiceReadModel)
async def create_invoice(
    session: AsyncSession = Depends(get_session), invoice: InvoiceReadModel = None
):

    existent_invoice: Invoice = await get_invoice_details_from_db(session, invoice.id)

    if existent_invoice:
        raise ValueError
    try:
        saved_invoice: Invoice = await save_invoice_to_db(session, invoice)
    except IntegrityError as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=InvoiceAlreadyExistsError.message,
        )
    return InvoiceReadModel.from_orm(**saved_invoice.__dict__)


"""Easy server running for debugging"""
if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000)
