import uuid
from typing import Union
from sqlalchemy import Column, Float
from sqlalchemy.dialects.postgresql import UUID

from infrastructure.database import Base


class Invoice(Base):
    __tablename__ = "invoice"

    id: Union[uuid.UUID, Column] = Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4
    )
    appointment_id: Union[uuid.UUID, Column] = Column(
        UUID(as_uuid=True), nullable=False, unique=True
    )
    total_price: Union[float, Column] = Column(Float)
