import uuid

from tests.conftest import client


def test_create_invoice():

    appointment_id = str(uuid.uuid4())
    response = client.post(
        "/invoice",
        json={"appointment_id": appointment_id, "total_price": 200},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["appointment_id"] == appointment_id
    assert "id" in data

    invoice_id = data["id"]

    response = client.get(
        f"/invoice/{invoice_id}",
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["appointment_id"] == appointment_id

    response = client.get(
        f"/invoice_by_appointment/{appointment_id}",
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["appointment_id"] == appointment_id
