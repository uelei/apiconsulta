from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select

from infrastructure.model import Invoice


async def get_invoice_details_from_db(session: AsyncSession, id: str) -> Invoice:

    query = await session.execute(select(Invoice).filter(Invoice.id == id))
    return query.scalars().first()


async def get_invoice_by_appointment_from_db(
    session: AsyncSession, appointment_id: str
) -> Invoice:

    query = await session.execute(
        select(Invoice).filter(Invoice.appointment_id == appointment_id)
    )
    return query.scalars().first()


async def save_invoice_to_db(
    session: AsyncSession, invoice: "InvoiceReadModel"
) -> Invoice:

    saved_invoice = Invoice(**invoice.__dict__)
    session.add(saved_invoice)

    await session.commit()

    return saved_invoice
